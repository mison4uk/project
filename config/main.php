<?php

return [
    'components' => [
        'router' => [
            //'class' => \Core\Routing\UrlRouter::class,
            'class' => \Core\Routing\Router::class,
            'file' => $_SERVER['DOCUMENT_ROOT'] . '/../app/Http/routes.php',
        ],
        'logger' => [
            'class' => \Core\Logging\Logger::class,
            'writer' => new \Core\Logging\FileWriter(),
            'formatter' => new \Core\Logging\StringFormatter(),
        ],
    ],
];