<?php
namespace  Core;

use Core\Logging\DbWriter;
use Core\Logging\FileWriter;
use Core\Logging\Logger;
use Core\Logging\StringFormatter;

use Core\contracts\RunnableInterface;
use Core\contracts\ContainerInterface;
use Core\contracts\BootstrapInterface;

class Application implements ContainerInterface, RunnableInterface, BootstrapInterface
{
    protected static $instance;

    protected $components = [];

    protected $config = [];

    private function __construct()
    {}

    private function __clone()
    {}

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function run()
    {
        if ($this->has('router')) {
            $action = $this->get('router')->route();
            if ($action) {
                $action();
            }
        }

        $this->logger->debug('Test message');
    }

    public function __get($name)
    {
        return $this->get($name);
    }

    public function __set($name, $value)
    {
        $this->components[$name] = $value;
    }

    public function configure($config)
    {
        $this->config = $config;

        $this->bootstrap();
    }

    public function bootstrap()
    {
        if (isset($this->config['components']) && is_array($this->config['components'])) {
            foreach ($this->config['components'] as $key => $component) {
                if (isset($component['class']) && class_exists($component['class'])) {
                    $className = $component['class'];
                    unset($component['class']);
                    $instance = new $className($component);
                    $this->components[$key] = $instance;
                }
            }
        }
    }

    public function has($name)
    {
        return array_key_exists($name, $this->components);
    }

    public function get($name)
    {
        return $this->components[$name] ?? null;
    }
}