<?php


namespace Core\Logging;


class FileWriter implements WriterInterface
{

    public function write($data)
    {
        file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/../storage/logs/app.log', $data . PHP_EOL, FILE_APPEND);
    }
}