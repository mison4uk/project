<?php


namespace Core\Routing;


interface RouterInterface
{
    public function route();
}