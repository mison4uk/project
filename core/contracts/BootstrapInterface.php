<?php
namespace Core\contracts;

interface BootstrapInterface
{
public function bootstrap();
}